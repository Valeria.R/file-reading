/*Assignment 3 
 * By Valeria Ramirez Osorio
 * Part 2
 */

//To start I imported java io and util for the File reading and other classes. 
import java.io.*;
import java.util.*;
public class part2 
{
	
//Set up the program 
	public static void main(String[] args) 
	{
		//Declare and add the file being used. This one used the text file called userData
		File file1 = new File("userData.txt");
		
		//I made a regular string array called array which will be used to hold the information after splitting the line
		String [] array = new String[3];
		
		//Then I made three array lists, each to hold a type of information from the lines
		//This first one will have all the names
		ArrayList<String> names = new ArrayList<String>();
		
		//This string will hold all the places
		ArrayList<String> place = new ArrayList<String>();
		
		//And this int will hold all the years they were born
		ArrayList<Integer> age = new ArrayList<Integer>();
		
		//int variable called sum, set to 0, which will be used to add up how many people will see the ad
		int sum = 0;
		
		//int variable names num, will be used to convert the years from string to int
		int num;
		
		//Start the file reading
		try
		{
			//Declare file reader and set the file it will read
			FileReader x = new FileReader(file1);
			
			//Declare buffered reader, naming it in, will be used to read lines 
			BufferedReader in = new BufferedReader(x);
			
			//Using the string variable line the first line of the text is read and saved under line
			String line = in.readLine();
			
			//A while loop that will run through the whole text, stopping only when there are no lines left, reach null
			while(line != null)
			{
				//Split the text in line by the commas and save all three parts in the array
				array =line.split(", ");
				
				//Takes the third position of the array, which would have saved the last part of the text 
				//and converts it into an int, saving it under num
				num = Integer.parseInt(array[2]);
				
				//Now, I store all the gathered information from the line and save it into the array lists
				//add the name, first position in array, under the names array list
				names.add(array[0]);
				
				//add the location, second position in array, under the place array list
				place.add(array[1]);
				
				//add the year, third position in array, under the age array list
				age.add(num);
		
				//Then it reads the next line
				line = in.readLine();
			}
			
			//Closes the try 
				in.close();
		}
		//Catches any exceptions 
		catch(IOException e)
		{
			
		}
		
		//This for loop will be used to check the data in the array lists to see who fits the conditions to view the ad
		//It will run the distance of the names array list, seeing as all lists should have the same size
		for(int counter = 0; counter < names.size(); counter++)
		{
			//This if statement checks two things, the age and the location. 
			//First it checks if the age is in the range of 2002-2007 because if you were born in these years, 
			//today you are in the age range of 13-18
			//Then it checks if the place is either mississauga or brampton
			//It moves along using the counter position to run along the lists
			
			if((age.get(counter) <= 2007 && age.get(counter) >= 2002) && (place.get(counter).equals("Mississauga") || place.get(counter).equals("Brampton")))
			{
			//If it passes both conditions, then it will print the username of the person
			System.out.println(names.get(counter) + " will see it.");
			//and it increases the sum variable
			sum++;
			}
			
		}

		//At the end it will print out how many people saw the ad by printing sum as the total number
		System.out.println("There will be "+ sum + " view(s) on the ad.");
		
	}

}
//End of Program


